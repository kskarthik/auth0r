/*
This program is written by Sai Karthik <kskarthik at disroot dot org>, Licensed under GPLv3
*/
package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base32"
	"fmt"
	"fyne.io/fyne"
	"github.com/spf13/viper"
	"os"
	"strings"
	"time"
	"fyne.io/fyne/app"
	"fyne.io/fyne/dialog"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
)

var pwd int

func main() {

	viper.AddConfigPath("$HOME/")
	viper.SetConfigName(".auth0r_config")
	viper.SetConfigType("toml")
	checkConf()
	gui()
	parseConfig()

}

/*
Check for config file. If not present, create one
*/
func checkConf() {
	// Switch to user's home directory
	usrHome, err := os.UserHomeDir()

	if err != nil {
		fmt.Println(err)
	}

	file := ".auth0r_config.toml"
	os.Chdir(usrHome)

	_, oerr := os.Open(file)

	if oerr != nil {

		fmt.Println(oerr, ". Creating one ...")

		_, ce := os.Create(file)

		if ce == nil {

			fmt.Println("Config file created")
			viper.Set("theme", true)
			viper.Set("c", 0)
			viper.WriteConfig()

		} else {
			fmt.Println("failed to create config file")
		}
	}
}

/*
   Main GUI
*/
func gui() {

	app := app.New()

	themeType := Theme()

	if themeType == true {

		app.Settings().SetTheme(theme.LightTheme())

	} else {

		app.Settings().SetTheme(theme.DarkTheme())
	}

	w := app.NewWindow("Auth0r - The 2FA application")
	w.Clipboard().Content()

	w.SetContent(widget.NewVBox(

		// New input box to create a new auth profile
		widget.NewToolbar(widget.NewToolbarAction(theme.ContentAddIcon(), func() {

			f := app.NewWindow("Form")

			name := widget.NewEntry()
			name.SetPlaceHolder("eg: Github")
			code := widget.NewEntry()
			code.SetPlaceHolder("Secret Code")

			form := &widget.Form{
				OnCancel: func() {
					w.Close()
				},
				OnSubmit: func() {
					// Dont add empty accounts
					if name.Text != "" && name.Text != "theme" && code.Text != "" {

						err := saveAccount(name.Text, code.Text)
						if err == 1 {

							dialog.ShowInformation("Oops!", "Failed to save details", w)

						} else {

							dialog.ShowInformation("Success!", "Details saved!", w)

						}

					} else {
						dialog.ShowInformation("Oops!", "Please enter a vaild account name / code", w)
					}

				},
			}
			form.Append("Account Name", name)
			form.Append("TOTP code", code)
			f.SetContent(form)
			f.Show()
		}),
			widget.NewToolbarSpacer(),
			// Theme selector
			widget.NewToolbarAction(theme.RadioButtonIcon(), func() {

				themeType := Theme()

				if themeType == true {

					app.Settings().SetTheme(theme.DarkTheme())
					viper.Set("theme", false)
					viper.WriteConfig()

				} else {
					app.Settings().SetTheme(theme.LightTheme())
					viper.Set("theme", true)
					viper.WriteConfig()
				}
			}),

			widget.NewToolbarAction(theme.QuestionIcon(), func() {

				dialog.ShowInformation("About Auth0r", "Developer: Sai Karthik\n\nLicense: GPLv3", w)
			}),
		),
		widget.NewLabel("Welcome to Auth0r! \n Click on '+' button to add a new account\n Click on O button to switch between light / dark theme"),
	))
	w.ShowAndRun()
	loadAcc()

}

/*
 Look for theme in config & return type
*/
func Theme() bool {

	Cerr := viper.ReadInConfig()

	if Cerr != nil {
		fmt.Println(Cerr)
	}

	return viper.GetBool("theme")
}

/*
 *  Save User entered account & passcode details to storage
 */
func saveAccount(acc, code string) int {

	viper.Set(acc, code)

	err := viper.WriteConfig()

	if err != nil {

		fmt.Print(err)
		return 1

	} else {

		return 0
	}
}

func loadAcc() *fyne.Widget {

	for k, v := range parseConfig() {

		if k != "theme" && k != "c" {

			var V interface{} = v
			fv := V.(string)

			widget.NewButton(k, func() { totp(fv) })

		}
	}
	return nil
}

/*
Parse Config file & return map
*/
func parseConfig() map[string]interface{} {

	var d (map[string]interface{})

	Cerr := viper.ReadInConfig()
	if Cerr != nil {
		fmt.Println(Cerr)

	} else {

		d = viper.AllSettings()

	}
	return d
}

// Imported code
func toBytes(value int64) []byte {
	var result []byte
	mask := int64(0xFF)
	shifts := [8]uint16{56, 48, 40, 32, 24, 16, 8, 0}
	for _, shift := range shifts {
		result = append(result, byte((value>>shift)&mask))
	}
	return result
}

func toUint32(bytes []byte) uint32 {
	return (uint32(bytes[0]) << 24) + (uint32(bytes[1]) << 16) +
		(uint32(bytes[2]) << 8) + uint32(bytes[3])
}

func oneTimePassword(key []byte, value []byte) uint32 {
	// sign the value using HMAC-SHA1
	hmacSha1 := hmac.New(sha1.New, key)
	hmacSha1.Write(value)
	hash := hmacSha1.Sum(nil)

	// We're going to use a subset of the generated hash.
	// Using the last nibble (half-byte) to choose the index to start from.
	// This number is always appropriate as it's maximum decimal 15, the hash will
	// have the maximum index 19 (20 bytes of SHA1) and we need 4 bytes.
	offset := hash[len(hash)-1] & 0x0F

	// get a 32-bit (4-byte) chunk from the hash starting at offset
	hashParts := hash[offset : offset+4]

	// ignore the most significant bit as per RFC 4226
	hashParts[0] = hashParts[0] & 0x7F

	number := toUint32(hashParts)

	// size to 6 digits
	// one million is the first number with 7 digits so the remainder
	// of the division will always return < 7 digits
	pwd := number % 1000000

	return pwd
}

// all []byte in this program are treated as Big Endian
func totp(code string) (otp uint32, timeLeft int64) {

	input := code

	// decode the key from the first argument
	inputNoSpaces := strings.Replace(input, " ", "", -1)
	inputNoSpacesUpper := strings.ToUpper(inputNoSpaces)
	key, err := base32.StdEncoding.DecodeString(inputNoSpacesUpper)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	// generate a one-time password using the time at 30-second intervals
	epochSeconds := time.Now().Unix()
	pwd := oneTimePassword(key, toBytes(epochSeconds/30))

	secondsRemaining := 30 - (epochSeconds % 30)
	//fmt.Printf("%06d (%d second(s) remaining)\n", pwd, secondsRemaining)

	return pwd, secondsRemaining
}
